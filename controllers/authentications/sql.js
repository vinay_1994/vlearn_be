'use strict';

module.exports = {
	insertRegisterOrg: 'INSERT INTO tbl_registration SET ?',
	isVerified: 'SELECT verified FROM tbl_registration WHERE contact_num = ?',
	updateOtpVerified: 'UPDATE tbl_registration SET verified = 1',
	login: 'SELECT * FROM tbl_registration WHERE contact_num = ?',
	updatePassword: 'UPDATE tbl_registration SET password = ? WHERE contact_num = ?',
};
