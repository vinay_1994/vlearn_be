'use strict';
const router = require('express').Router();
const httpUtil = require('../../common/utilities/httputil');
const query = require('./sql');
const dbCon = require('../../common/utilities/dbconnection');
const smsUtil = require('./../../common/utilities/sendSms');
const jwtUtil = require('../../common/utilities/jwtUtil');

//login handler
router.post('/register', async (req, res) => {
	try {
		let data = await dbCon.query(query.login, req.body.contact_num);
		if (data.length === 0) {
			smsUtil.verify.request(
				{
					number: `${req.body.country_code}${req.body.contact_num}`,
					brand: 'VLEARN',
					code_length: '4',
				},
				async (err, result) => {
					if (err) {
						res.json(httpUtil.getException('something went wrong'));
					} else if (result && result.status == '0') {
						await dbCon.query(query.insertRegisterOrg, req.body);
						res.json(httpUtil.getSuccess(result, 'Otp has been sent'));
					} else {
						res.json(httpUtil.getError('Error Sending Otp', result));
					}
				}
			);
		} else {
			res.json(httpUtil.getBadRequest('user exists kindly login'));
		}
	} catch (error) {
		console.log(error);
		res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

router.post('/verify', async (req, res) => {
	let otp = req.body.otp;
	let requestId = req.body.request_id;
	try {
		smsUtil.verify.check({ request_id: requestId, code: otp }, async (err, result) => {
			if (err) {
				res.json(httpUtil.getException('something went wrong', err));
			} else {
				if (result && result.status == '0') {
					await dbCon.query(query.updateOtpVerified);
					res.json(httpUtil.getSuccess(null, 'otp verified successfully'));
				}
			}
		});
	} catch (error) {
		res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

router.post('/login', async (req, res) => {
	try {
		let data = await dbCon.query(query.login, req.body.contact_num);
		if (data.length !== 0) {
			if (data[0].password === req.body.password) {
				if (data[0].verified === 1) {
					jwtUtil.signToken(data[0].contact_num, data[0].id, (token) => {
						res.json(httpUtil.getSuccess({ token }, 'logged in successfully'));
					});
				} else {
					res.json(httpUtil.getBadRequest('user not verified'));
				}
			} else {
				res.json(httpUtil.getBadRequest('wrong password'));
			}
		} else {
			res.json(httpUtil.getBadRequest('user does not exist'));
		}
	} catch (error) {
		res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

router.post('/forgot_password', async (req, res) => {
	try {
		let data = await dbCon.query(query.isVerified, req.body.contact_num);
		if (data.length === 1) {
			smsUtil.verify.request(
				{
					number: `${req.body.country_code}${req.body.contact_num}`,
					brand: 'VLEARN',
					code_length: '4',
				},
				async (err, result) => {
					if (err) {
						res.json(httpUtil.getException('something went wrong'));
					} else if (result && result.status == '0') {
						res.json(httpUtil.getSuccess(result, 'Otp has been sent'));
					} else {
						res.json(httpUtil.getError('Error Sending Otp', result));
					}
				}
			);
		} else {
			res.json(httpUtil.getBadRequest('Not Registered', null));
		}
	} catch (error) {
		res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

router.post('/change_password', async (req, res) => {
	try {
		let data = await dbCon.query(query.isVerified, req.body.contact_num);
		if (data.length === 1) {
			await dbCon.query(query.updatePassword, [req.body.password, req.body.contact_num]);
			res.json(httpUtil.getSuccess(null, 'Password Changed'));
		} else {
			res.json(httpUtil.getBadRequest('Not Registered', null));
		}
	} catch (error) {
		res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

module.exports = router;
