'use strict';
const router = require('express').Router();
const api = require('../Api/baseurls');
const authController = require('./authentications');
const dashBoardController = require('./dashboardcontroller');

const tokenCheck = require('../common/middlewares/tokenCheck');

router.use(api.auth, authController);
router.use(api.dashboard, tokenCheck, dashBoardController);

module.exports = router;
