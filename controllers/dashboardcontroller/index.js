'use strict';
const router = require('express').Router();
const api = require('../../Api/baseurls');
const homeController = require('./Home');
const shareController = require('./Sharecontroller');

router.use(api.root, homeController);
router.use(api.share, shareController);

module.exports = router;
