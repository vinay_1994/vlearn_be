'use strict';
const sqlQuery = require('./sql');
const dbCon = require('../../common/utilities/dbconnection');

exports.joinChatRequest = async reqDetails => {
	try {
		let user = await dbCon.query(sqlQuery.chatUserCheck, reqDetails.userMail);
		return true;
	} catch (error) {
		throw error;
	}
};
