'use strict';

module.exports = {
	getUsersList: 'SELECT *  FROM tbl_registration WHERE verified = 1',
	getMediaDetails: 'SELECT * FROM tbl_media_details WHERE media_id = ?',
	addSharedData: 'INSERT INTO tbl_shared_details SET ?',
	checkIsShared: 'SELECT * FROM tbl_shared_details WHERE shared_file_id = ? AND shared_to_id = ?',
	getSharedContents: 'SELECT * FROM tbl_shared_details WHERE shared_to_id = ?',
	removeSharedFile: 'DELETE FROM tbl_shared_details WHERE shared_file_id = ? AND shared_to_id = ?',
};
