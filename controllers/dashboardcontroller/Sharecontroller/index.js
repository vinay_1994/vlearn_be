'use strict';
const router = require('express').Router();
const httpUtil = require('../../../common/utilities/httputil');
const s3Upload = require('./../../../common/utilities/fileUploadtoS3');
const sql = require('./sql');
const dbCon = require('../../../common/utilities/dbconnection');

router.get('/', async (req, res) => {
	try {
		let data = await dbCon.query(sql.getSharedContents, req.body.tokenDetails.id);
		let resObj = { AUDIO: [], VIDEO: [] };
		if (data.length !== 0) {
			data.forEach((i) => {
				if (i.media_type === 'audio') {
					resObj.AUDIO.push({
						media_id: i.shared_file_id,
						original_filename: i.original_filename,
						file_size: i.file_size,
					});
				} else if (i.media_type === 'video') {
					resObj.VIDEO.push({
						media_id: i.shared_file_id,
						original_filename: i.original_filename,
						file_size: i.file_size,
					});
				}
			});
		}
		res.json(httpUtil.getSuccess(resObj, 'successfully fetched data'));
	} catch (error) {
		console.log(error);
		return res.json(httpUtil.getException(error));
	}
});

router.get('/getuserslist', async (req, res) => {
	try {
		let data = await dbCon.query(sql.getUsersList);
		data.forEach((i) => {
			delete i['password'];
		});
		res.json(httpUtil.getSuccess(data, 'successfully fetched data'));
	} catch (error) {
		console.log(error);
		return res.json(httpUtil.getException(error));
	}
});

router.post('/shareto/:id', async (req, res) => {
	try {
		let isShared = await dbCon.query(sql.checkIsShared, [req.params.id, req.body.id]);
		if (isShared.length === 0) {
			let data = await dbCon.query(sql.getMediaDetails, req.params.id);
			if (data.length === 1) {
				let x = {};
				x.file_key = data[0].file_key;
				x.file_bucket = data[0].file_bucket;
				x.shared_file_id = req.params.id;
				x.file_owner_id = req.body.tokenDetails.id;
				x.shared_to_id = req.body.id;
				x.file_owner_number = req.body.tokenDetails.tempUser;
				x.shared_to_number = req.body.contact_num;
				x.media_type = data[0].media_type;
				x.original_filename = data[0].original_filename;
				x.file_size = data[0].file_size;
				await dbCon.query(sql.addSharedData, x);
				res.json(httpUtil.getSuccess('shared successfully', null));
			} else {
				res.json(httpUtil.getError('duplicated files', null));
			}
		} else {
			res.json(httpUtil.getError('files already shared', null));
		}
	} catch (error) {
		return res.json(httpUtil.getException('something went wrong', error.toString()));
	}
});

router.post('/removeShared/:id', async (req, res) => {
	try {
		let isShared = await dbCon.query(sql.checkIsShared, [req.params.id, req.body.id]);
		if (isShared.length === 0) {
			res.json(httpUtil.getError('file has not been shared', null));
		} else if (isShared.length === 1) {
			await dbCon.query(sql.removeSharedFile, [req.params.id, req.body.id]);
			res.json(httpUtil.getSuccess('file unshared success fully', null));
		} else {
			res.json(httpUtil.getError('error occured', null));
		}
	} catch (error) {
		return res.json(httpUtil.getException('something went wrong', error.toString()));
	}
});

module.exports = router;
