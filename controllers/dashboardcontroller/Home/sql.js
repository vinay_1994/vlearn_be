'use strict';

module.exports = {
	addMedia: 'INSERT INTO tbl_media_details SET ?',
	getDashboardContent: 'SELECT * FROM tbl_media_details WHERE user_id = ?',
	getMediaPath: 'SELECT * FROM tbl_media_details WHERE user_id = ? AND media_id = ?',
};
