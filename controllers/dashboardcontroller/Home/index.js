'use strict';
const router = require('express').Router();
const config = require('config');
const httpUtil = require('../../../common/utilities/httputil');
const S3Util = require('./../../../common/utilities/fileUploadtoS3');
const sql = require('./sql');
const dbCon = require('../../../common/utilities/dbconnection');

router.get('/', async (req, res) => {
	try {
		let data = await dbCon.query(sql.getDashboardContent, req.body.tokenDetails.tempUser);
		let resObj = { AUDIO: [], VIDEO: [] };
		if (data.length !== 0) {
			data.forEach((i) => {
				if (i.media_type === 'audio') {
					resObj.AUDIO.push({
						media_id: i.media_id,
						original_filename: i.original_filename,
						file_size: i.file_size,
						is_owner: i.is_owner,
					});
				} else if (i.media_type === 'video') {
					resObj.VIDEO.push({
						media_id: i.media_id,
						original_filename: i.original_filename,
						file_size: i.file_size,
						is_owner: i.is_owner,
					});
				}
			});
		}
		res.json(httpUtil.getSuccess(resObj, 'successfully fetched data'));
	} catch (error) {
		return res.json(httpUtil.getException('somehing went wrong', error.toString()));
	}
});

router.post('/upload', async (req, res) => {
	try {
		let mediaDetails = { user_id: req.body.tokenDetails.tempUser };
		await S3Util.setBody(req.body);
		const singleUpload = S3Util.upload.single('file');
		singleUpload(req, res, async function (err) {
			if (err) {
				res.json(httpUtil.getError(err.message));
			} else {
				mediaDetails.media_type = req.file.mimetype.split('/')[0];
				mediaDetails.original_filename = req.file.originalname;
				mediaDetails.file_size = req.file.size;
				mediaDetails.is_owner = 1;
				mediaDetails.file_key = req.file.key;
				mediaDetails.file_bucket = req.file.bucket;
				mediaDetails.location = req.file.location;
				await dbCon.query(sql.addMedia, mediaDetails);
				res.json(httpUtil.getSuccess(null, 'Uploaded successfully'));
			}
		});
	} catch (error) {
		return res.json(httpUtil.getException('somrthing went wrong', error.toString()));
	}
});

router.get('/getstreem/:id', async (req, res) => {
	try {
		if (req.params.id) {
			let data = await dbCon.query(sql.getMediaPath, [req.body.tokenDetails.tempUser, req.params.id]);
			if (data.length !== 0) {
				var s3Stream = S3Util.S3.getObject({
					Bucket: `${data[0].file_bucket}`,
					Key: `${data[0].file_key}`,
				}).createReadStream();
				s3Stream.on('error', function (err) {
					//res.json(httpUtil.getError('error', err.toString()));
				});
				s3Stream
					.pipe(res)
					.on('error', function (err) {
						//res.json(httpUtil.getSuccess(err.toString(), 'Error Happend'));
					})
					.on('close', function () {
						//res.json(httpUtil.getSuccess(null, 'Play back completed'));
					});
			} else {
				res.json(httpUtil.getError('media does not exist', null));
			}
		} else {
			res.json(httpUtil.getBadRequest('missing media id', null));
		}
	} catch (error) {
		return res.json(httpUtil.getException('internal server error', error.toString()));
	}
});

module.exports = router;
