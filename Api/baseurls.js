module.exports = {
	root: '/',
	root_url: '/api/1.0/',
	auth: '/auth',
	dashboard: '/dashboard',
	share: '/shr',
};
