const express = require('express');
const controllers = require('./controllers');
const api = require('./Api/baseurls');
const options = require('./common/middlewares/corsmiddleware');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(api.root_url, controllers);

app.use(function (err, req, res, next) {
	res.status(err.status || 500);
	res.json('error');
});

process.on('unhandledRejection', function () {
	console.log('unhandeled rejection occured');
});

app.listen(5000, function () {
	console.log('listening on 5000');
});
