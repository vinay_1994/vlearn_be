'use strict';
const jwtUtil = require('./../utilities/jwtUtil');
const httpUtil = require('./../utilities/httputil');

module.exports = (req, res, next) => {
	try {
		let token = req.headers.authorization.split(' ')[1];
		if (token) {
			jwtUtil.verifyToken(token, (decodeToken) => {
				req.body.tokenDetails = decodeToken;
				next();
			});
		} else {
			res.json(httpUtil.getUnauthorized());
		}
	} catch (error) {
		res.json(httpUtil.getUnauthorized());
	}
};
