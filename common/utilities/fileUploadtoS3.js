'use strict';
const AWS = require('aws-sdk');
const config = require('config');
const multer = require('multer');
const multerS3 = require('multer-s3');

const s3 = new AWS.S3({
	accessKeyId: config.get('s3.ACCESS_KEY'),
	secretAccessKey: config.get('s3.SECREATE_KEY'),
	region: config.get('s3.region'),
});

const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'audio/mpeg' || file.mimetype === 'video/mp4' || file.mimetype === 'image/jpeg') {
		cb(null, true);
	} else {
		cb(new Error('Invalid file type, only JPEG  MP4 And Mp3 is allowed!'), false);
	}
};

let reqDetails = null;

exports.upload = multer({
	fileFilter,
	storage: multerS3({
		acl: 'public-read',
		s3,
		bucket: function (req, file, cb) {
			cb(null, config.get('s3.bucket_name') + '/' + reqDetails.tempUser + '/' + file.mimetype.split('/')[0]);
		},
		metadata: function (req, file, cb) {
			cb(null, { fieldName: file.fieldname });
		},
		key: function (req, file, cb) {
			cb(
				null,
				Date.now().toString() + '.' + file.originalname.split('.')[0] + '.' + file.originalname.split('.')[1]
			);
		},
	}),
});

exports.setBody = (req) => {
	reqDetails = req.tokenDetails;
	return;
};

exports.S3 = s3;
