'use strict';
const config = require('config');
const Nexmo = require('nexmo');

module.exports = new Nexmo({
	apiKey: config.get('nexmo.apiKey'),
	apiSecret: config.get('nexmo.apiSecret'),
});
