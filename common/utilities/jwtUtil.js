'use strict';
const JWT = require('jsonwebtoken');
const PrivateKey = 'withmemoryofkulli';

exports.signToken = (jwtData, id, cb) => {
	JWT.sign({ tempUser: jwtData, id }, PrivateKey, { expiresIn: '24h' }, (err, token) => {
		if (!err) {
			cb(token);
		} else {
			throw 'Error in setting token';
		}
	});
};

exports.verifyToken = (token, cb) => {
	JWT.verify(token, PrivateKey, (err, data) => {
		if (!err) {
			cb(data);
		} else {
			throw 'Token Expired';
		}
	});
};
